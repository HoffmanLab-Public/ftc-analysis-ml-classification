clc
clear
close all
addpath(genpath(pwd));
addpath('../export_fig')
folder = input('Which folder do you want to analyze? ','s');

%% Import Parameters Required for Data Analysis
[EXPp,FFCp] = BuildAnalysisParams(folder);

%% Import appropriate model for AP calculations
APmodel = ImportAPmodel(FFCp);

%% Import dataset & add UniqueID columns
[d,CN] = ImportDataset(EXPp,FFCp);

%% Clean data with universal exclusion criteria
d = CleanData(d,CN,EXPp,FFCp);

%% Calculate StainIntensity
[d,CN] = CalcStainIntensity(d,CN,FFCp);

%% Prepare for AP and RI binning and calculations
[d,CN,FFCp] = APcalcPrep(APmodel,d,CN,FFCp);
FFCp = RIcalcPrep(d,CN,FFCp);

%% AP & RI binning assignments
[d,CN] = APcalc(d,CN,APmodel,FFCp);
if strcmpi(EXPp.Construct,'VinTS')
    [d,CN] = RIcalc(d,CN,FFCp.RIquartiles);
    [d,CN,FFCp] = APsRIcalc(d,CN,FFCp);
end

%% Visualize CC, AP, RI, and/or APsRI cell-by-cell or globally
CC_AP_RI_APsRI_visualization

%% Save out updated FFCp
writetable(struct2table(FFCp),fullfile(FFCp.folder,'UpdatedFFCp.txt'));
writetable(struct2table(EXPp),fullfile(FFCp.folder,'UpdatedEXPp.txt'));

%% Clean up workspace
rmpath(FFCp.SaveFolder);
rmpath(genpath(folder));
rmpath(genpath(pwd));
rmpath('../export_fig');