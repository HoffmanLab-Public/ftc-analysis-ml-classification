function [d,CN] = APcalc(d,CN,APmodel,FFCp)
%APcalc Adds architectural preference column to dataset "d" based on APmodel

switch FFCp.ClassificationType
case 'Gradient'
    % Use classic FRET-gradient method to determine Architectural preference (AP)
    HighFRETrows = d.num(d.num(:,CN.eff.num)>FFCp.RIquartiles(3),:); % isolate high tension (low FRET) FAs
    CentralFArows = d.num(d.num(:,CN.DP.num)>FFCp.DPcutoff,:); % isolate peripheral (in terms of distance parameter, DP) FAs
    NonRadialFAs = d.num(d.num(:,CN.DP.num)>FFCp.RadialFA,:); % isolate radially oriented FAs
    ShortFAs = d.num(d.num(:,CN.AR.num)<FFCp.LongFA,:); % isolate longer FAs for more confident gradient calculation
    USrows = HighFRETrows | CentralFArows | NonRadialFAs | ShortFAs;
    SFrows = d.num(:,CN.FAgrad.num)>APmodel(1) & d.num(:,CN.FAgrad.num)<APmodel(2) & ~USrows;
    LProws = d.num(:,CN.FAgrad.num)>APmodel(3) & d.num(:,CN.FAgrad.num)<APmodel(4)  & ~USrows;
    UKrows1 = d.num(:,CN.FAgrad.num)>=APmodel(2) & d.num(:,CN.FAgrad.num)<=APmodel(3) & ~USrows;
    UKrows2 = d.num(:,CN.FAgrad.num)>=APmodel(4) & d.num(:,CN.FAgrad.num)<=APmodel(5) & ~USrows;
    UKrows = UKrows1 | UKrows2;
case 'AndyManual'
    SFrows1 = d.num(:,APmodel.Size.num)>APmodel.Size.SFlim(1) & d.num(:,APmodel.Size.num)<APmodel.Size.SFlim(2);
    SFrows2 = d.num(:,APmodel.AR.num)>APmodel.AR.SFlim(1) & d.num(:,APmodel.AR.num)<APmodel.AR.SFlim(2);
    SFrows3 = d.num(:,APmodel.FArad.num)>APmodel.FArad.SFlim(1) & d.num(:,APmodel.FArad.num)<APmodel.FArad.SFlim(2);
    SFrows4 = d.num(:,APmodel.GRD.num)>APmodel.GRD.SFlim(1) & d.num(:,APmodel.GRD.num)<APmodel.GRD.SFlim(2);
    SFrows5 = d.num(:,APmodel.FourNN.num)>APmodel.FourNN.SFlim(1) & d.num(:,APmodel.FourNN.num)<APmodel.FourNN.SFlim(2);
    SFrows = SFrows1 &  SFrows2 & SFrows3 & SFrows4 & SFrows5;

    LProws1 = d.num(:,APmodel.Size.num)>APmodel.Size.LPlim(1) & d.num(:,APmodel.Size.num)<APmodel.Size.LPlim(2);
    LProws2 = d.num(:,APmodel.AR.num)>APmodel.AR.LPlim(1) & d.num(:,APmodel.AR.num)<APmodel.AR.LPlim(2);
    LProws3 = d.num(:,APmodel.FArad.num)>APmodel.FArad.LPlim(1) & d.num(:,APmodel.FArad.num)<APmodel.FArad.LPlim(2);
    LProws4 = d.num(:,APmodel.GRD.num)>APmodel.GRD.LPlim(1) & d.num(:,APmodel.GRD.num)<APmodel.GRD.LPlim(2);
    LProws5 = d.num(:,APmodel.FourNN.num)>APmodel.FourNN.LPlim(1) & d.num(:,APmodel.FourNN.num)<APmodel.FourNN.LPlim(2);
    LProws = LProws1 &  LProws2 & LProws3 & LProws4 & LProws5;

    UKrows = ~SFrows | ~LProws;
    USrows = false(size(UKrows));
case 'SVM'
    if strcmpi(FFCp.Style,'1v1')
        vaX = d.num(:,CN.pCols);
        [~,score1] = predict(APmodel.LPvsSF,vaX);
        [~,score2] = predict(APmodel.LPvsUK,vaX);
        [~,score3] = predict(APmodel.SFvsUK,vaX);
        SFrows = score1(:,2)>0 & score3(:,1)>FFCp.StringencySF;
        LProws = score1(:,1)>0 & score2(:,1)>FFCp.StringencyLP;
        UKrows = score2(:,2)>-FFCp.StringencyLP & score3(:,2)>-FFCp.StringencySF;
    elseif strcmpi(FFCparam.Style,'1vAll')
        error('Train these types of models with the rac supplemented dataset')
    else
        error('Choose a valid Classification/Style combo');
    end
    USrows = ~SFrows & ~LProws & ~UKrows; % unsure rows
case 'Tree'
    vaX = d.num(:,CN.pCols);
    [~,score] = predict(APmodel.ens,vaX);
    mscore = max(score,[],2);
    SFrows = score(:,1) == mscore;
    LProws = score(:,2) == mscore;
    UKrows = score(:,3) == mscore;
    USrows = ~SFrows & ~LProws & ~UKrows; % unsure rows
otherwise
    error('Choose a valid Classification method for AP calcs');
end

%% Add column for classification of architecture
d.text(SFrows,end+1) = {'SF'};
d.num(SFrows,end+1) = 1;
d.text(LProws,end) = {'LP'};
d.num(LProws,end) = 2;
d.text(UKrows,end) = {'UK'};
d.num(UKrows,end) = 0;
d.text(USrows,end) = {'US'};
d.num(USrows,end) = 3;
d.headers{end+1} = 'Architecture';
[~,CN.Arch.num] = size(d.num);
CN.Arch.text = 'Architecture';