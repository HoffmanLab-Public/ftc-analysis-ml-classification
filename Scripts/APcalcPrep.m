function [d,CN,FFCp] = APcalcPrep(APmodel,d,CN,FFCp)
%APcalcPrep Prepares the dataset for classification analysis and
%           architectural preference calculations with the following
%           operations
%            (1) Standardize predictors ('Tree' only...will make new dataset)
%            (2) Determine quantitative numbers for stringency ('SVM' only)

%% Determine quantitative numbers for stringency
% Calculate model bining before entering into cell-by-cell calculations
if strcmpi(FFCp.ClassificationType,'SVM')
    vaX = d.num(:,CN.pCols);
    switch FFCp.Style
        case '1v1'
            [~,SFscores] = predict(APmodel.SFvsUK,vaX);
            [~,LPscores] = predict(APmodel.LPvsUK,vaX);
        case '1vAll'
            error('Train these types of models with the rac supplemented dataset')
    end
    SFscores(SFscores>10)=10;
    SFscores(SFscores<-10)=-10;
    LPscores(LPscores>10)=10;
    LPscores(LPscores<-10)=-10;
    SFscores = reshape(SFscores,[length(SFscores)*2,1]);
    LPscores = reshape(LPscores,[length(LPscores)*2,1]);
    SFq = quantile(SFscores,199); % 1% increments in SF scores
    LPq = quantile(LPscores,199); % 1% increments in LP scores
    SFqloc = 100+100*FFCp.Stringency;
    LPqloc = 100+100*FFCp.Stringency;
    FFCp.StringencySF = SFq(SFqloc);
    FFCp.StringencyLP = LPq(LPqloc);
end
%% Standardize predictors
% CN.pCols; % predictor column numbers
if strcmpi(FFCp.ClassificationType,'Tree')
    npred = length(CN.pCols);
    for i = 1:npred
        d.num(:,end+1) = zscore(d.num(:,CN.pCols(i)));
        d.text = horzcat(d.text,cell([length(d.text),1])); % empty text
        d.headers{end+1} = ['stdz_',FFCp.Predictors{i}];
        CN.(d.headers{end}).num = length(d.headers);
        CN.(d.headers{end}).text = ['standardized ', CN.(FFCp.Predictors{i}).text];
        FFCp.Predictors{i} = d.headers{end};
        CN.pCols(i) = length(d.headers);
        CN.nCols = CN.nCols+1;
    end
    warning('Predictor data were standardized for tree-based classification');
end
end

