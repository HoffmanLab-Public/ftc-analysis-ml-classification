function [d,CN,FFCp] = APsRIcalc(d,CN,FFCp)
%APsRIcalc adds APsRI column to "d" based on computed APsRIquartiles
%          APsRI = Architectural Preference Specific Recruitment Index

%% Determine bins for APsRI FAs
arch = d.num(:,CN.Arch.num);
eff = d.num(:,CN.eff.num);
SFrows = arch==1;
LProws = arch==2;
APsRIqRows = SFrows | LProws;
APsRIq = quantile(eff(APsRIqRows),3);
APsRIq = [5 APsRIq 40];
FFCp.APsRIquartiles = APsRIq;

%comment this out or the one above based on how we want the quantile to be
%based on
%FFCp.APsRIquartiles = FFCp.RIquartiles;

SF_HighForceRows = SFrows & eff>APsRIq(1) & eff<APsRIq(2);
SF_IntForceRows = SFrows & eff>=APsRIq(2) & eff<=APsRIq(4);
SF_LowForceRows = SFrows & eff>APsRIq(4) & eff<APsRIq(5);
LP_HighForceRows = LProws & eff>APsRIq(1) & eff<APsRIq(2);
LP_IntForceRows = LProws & eff>=APsRIq(2) & eff<=APsRIq(4);
LP_LowForceRows = LProws & eff>APsRIq(4) & eff<APsRIq(5);

%% Add column for SF specific RI bins
d.text(:,end+1) = {'NotSF'};
d.num(:,end+1) = NaN;
d.text(SF_HighForceRows,end) = {'SFlowFRET'};
d.num(SF_HighForceRows,end) = 1;
d.text(SF_IntForceRows,end) = {'SFmediumFRET'};
d.num(SF_IntForceRows,end) = 0;
d.text(SF_LowForceRows,end) = {'SFhighFRET'};
d.num(SF_LowForceRows,end) = 2;
d.headers{end+1} = 'SFbinnedFRET';
[~,CN.SFbinnedFRET.num] = size(d.num);
CN.SFbinnedFRET.text = 'SF Binned FRET efficiency';

%% Add column for LP specific RI bins
d.text(:,end+1) = {'NotLP'};
d.num(:,end+1) = NaN;
d.text(LP_HighForceRows,end) = {'LPlowFRET'};
d.num(LP_HighForceRows,end) = 1;
d.text(LP_IntForceRows,end) = {'LPmediumFRET'};
d.num(LP_IntForceRows,end) = 0;
d.text(LP_LowForceRows,end) = {'LPhighFRET'};
d.num(LP_LowForceRows,end) = 2;
d.headers{end+1} = 'LPbinnedFRET';
[~,CN.LPbinnedFRET.num] = size(d.num);
CN.LPbinnedFRET.text = 'LP Binned FRET efficiency';
end

