function [EXPp,FFCp] = BuildAnalysisParams(folder)
%BuildAnalysisParams.m
% Imports, parses, and builds two structures that define the image or data
% analysis pipeline for fluorescence-force colocalization datasets
% 1: EXPp = define what constructs/costains/days of data are to be analyzed
% 2: FFCp = define the models to be used to identify low/med/high tension
%           FAs and SF/LP/UK type FAs for calculation of Recruitment Index
%           and Architectural Preference

%% Build FFC Analysis parameters
addpath(folder)
params_file = file_search('FFC_Param\w+.txt',folder);
FFCparam.folder = folder;
fid = fopen(params_file{1});
while ~feof(fid)
    aline = fgetl(fid);
    eval(aline)
end
fclose(fid);

%% Build EXP Analysis parameters
params_file = file_search('Exp_Param\w+.txt',folder);
fid = fopen(params_file{1});
while ~feof(fid)
    aline = fgetl(fid);
    aline = strrep(aline,'save_','EXPparam.'); % convert saveparams to structure
    if contains(aline,'EXPparam')
        eval(aline)
    end
end
fclose(fid);

% Rename and rearrange/add to EXP and FFC params
FFCp = FFCparam;
EXPp = EXPparam;
EXPp.date = [EXPp.date(1:4),'-',EXPp.date(5:6),'-',EXPp.date(7:8)];

% Basename for saving new files
FFCp.SaveName = strcat(...
    FFCp.ClassificationType,'_',...
    FFCp.Style,'_',...
    FFCp.Weights,'weighted_',...
    strrep(strrep(num2str(FFCp.Stringency,'%2.2f'),'-','neg'),'.','p'),'stringency_',...
    strrep(FFCp.StainIntensity,'/','PER'),'_mg',...
    FFCp.MediumGroup);

% Where to save xlsx and/or imgs and plots
FFCp.SaveFolder = fullfile(folder,'CC_AP_RI_APsRI_plots');
if ischar(EXPp.Costain)
    FFCp.SaveFolder1 = fullfile(folder,'RepImgs');
end

if exist(FFCp.SaveFolder,'dir')==7
    warning('you are saving to a pre-existing folder, some files may be overwritten');
else
    mkdir(FFCp.SaveFolder)
    addpath(FFCp.SaveFolder)
end

if ischar(EXPp.Costain)
    if exist(FFCp.SaveFolder1,'dir')==7
        warning('you are saving to a pre-existing folder, some files may be overwritten');
    else
        mkdir(FFCp.SaveFolder1)
        addpath(FFCp.SaveFolder1)
    end
end
