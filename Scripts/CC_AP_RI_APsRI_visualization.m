% CC_AP_RI_visualization is a wrapper function that determines whether
%                        cell-by-cell or global visualization of correlation
%                        coefficients (CC), architectural preference (AP) or
%                        recruitment index (RI) is warranted for the dataset
%                        of interest (d) with given FFC and EXP parameters
%
% First calculate correlation coefficient
%    CC
%
% Second, isolate mean, std, nCellsContrib to stain intensity
% in the following groups of FAs:
%    I_SFs  vs  <I_SFs>
%    I_LPs  vs  <I_LPs>
%    I_HighTension  vs  I_LowTension
%    I_SFs_HighTension  vs  I_SFs_LowTension
%    I_LPs_HighTension  vs  I_LPs_LowTension

%% Before anything, save out current dataset
chcols = logical(cellfun('length',d.text (1,:)));
outdata = num2cell(d.num);
outdata(:,chcols) = d.text(:,chcols);
outdata = vertcat(d.headers,outdata);
xlswrite(fullfile(FFCp.SaveFolder,'cleaned_and_binned_dataset.xlsx'),outdata);

%% For either single or multiple costains, perform global visualizations with stats
[~,COSidx]=ismember(uniqueRowsCA(d.text(:,CN.COS.num)),EXPp.Costain);
us = unique(d.num(:,CN.COS.num));
if strcmpi(EXPp.Construct,'VinTS')
    BinCols = [CN.Arch.num, CN.FRETbins.num, CN.SFbinnedFRET.num,CN.LPbinnedFRET.num];
else
    BinCols = [CN.Arch.num];
end
nquant = length(BinCols);
BinGroups = cell([nquant,1]);
SaveName{1} = {'AP'};
BinGroups{1} = {'UK',       'SF',        'LP'};
if strcmpi(EXPp.Construct,'VinTS')
    SaveName{2} = {'RI'};
    SaveName{3} = {'SFsRI'};
    SaveName{4} = {'LPsRI'};
    BinGroups{2} = {'MedFRET',  'LowFRET',  'HighFRET'};
    BinGroups{3} = {'SFmedFRET','SFlowFRET','SFhighFRET'};
    BinGroups{4} = {'LPmedFRET','LPlowFRET','LPhighFRET'};
end
GlobalQuant = cell([nquant*3+1,1]);
GlobalQuantHeaders = cell([nquant*3+1,1]);
od = cell([nquant*3,1]);
n = 0;
for i = 1:nquant
    for j = 1:3
        n = n+1;
        GlobalQuant{n} = zeros([length(us),4]);
        for s = 1:length(COSidx)
            idx = COSidx(s);
            Srows = d.num(:,CN.COS.num)==us(s);
            Brows = d.num(:,BinCols(i))==j-1;
            rows = Srows & Brows;
            %%%%% cell by cell %%%%%%
            uc = unique(d.num(rows,CN.Unique.num));
            tmp = zeros([length(uc),6]);
            for c = 1:length(uc)
                crows = rows & d.num(:,CN.Unique.num)==uc(c);
                tmp(c,1) = mean(d.num(crows,CN.stain_intensity.num));
                tmp(c,2) = std(d.num(crows,CN.stain_intensity.num));
                tmp(c,3) = length(nonzeros(crows));
                tmp(c,4) = mean(d.num(crows,CN.ImgID.num));
                tmp(c,5) = mean(d.num(crows,CN.CellID.num));
                tmp(c,6) = mean(str2double(strrep(d.text(crows,CN.Date.num),'/','')));
            end
            if strcmpi(FFCp.Weights,'y')
                weights = tmp(:,3);
            else
                weights = ones([length(tmp),1]);
            end
            GlobalQuant{n}(idx,1) = wmean(tmp(:,1),weights,1); % mean
            GlobalQuant{n}(idx,2) = wstd(tmp(:,1),weights); % std OF THE MEAN
            GlobalQuant{n}(idx,3) = length(tmp); % nCells
            GlobalQuant{n}(idx,4) = sum(tmp(:,3)); % nFAs
            %%%%%%%%%%%%%%%%%%%%%%%%%
%             GlobalQuant{c}(idx,1) = mean(d.num(rows,CN.stain_intensity.num)); % mean
%             GlobalQuant{c}(idx,2) = std(d.num(rows,CN.stain_intensity.num)); % std
%             GlobalQuant{c}(idx,3) = length(unique(d.num(rows,CN.Unique.num))); % nCells
%             GlobalQuant{c}(idx,4) = length(nonzeros(rows)); % nFAs
            GlobalQuantHeaders{n} = {...
                ['mean_',BinGroups{i}{j},'_intensity'],...
                ['std_',BinGroups{i}{j},'_intensity'],...
                ['nCells_',BinGroups{i}{j},'_group'],...
                ['nFAs_',BinGroups{i}{j},'_group']};
            if n==1
                % calculate correlation coefficient at GlobalQuant(end+1)
                [GlobalQuant{end}(idx,:),GlobalQuantHeaders{end}] = VisualizeCCglobal(d.num(Srows,:),CN);
            end
            tmp(:,end+1) = us(s);
            if s == 1
                od{n} = tmp;
            else
                od{n} = vertcat(od{n},tmp);
            end
        end
    end
    [u1,~,~] = unique(od{n-2}(:,4:7),'rows'); % med group
    [u2,~,~] = unique(od{n-1}(:,4:7),'rows'); % low group
    [u3,~,~] = unique(od{n}(:,4:7),'rows'); % high group
    [u,~,~] = unique([u1;u2;u3],'rows'); % all unique cells
    [~,ia1,ic1] = intersect(u,u1,'rows');
    [~,ia2,ic2] = intersect(u,u2,'rows');
    [~,ia3,ic3] = intersect(u,u3,'rows');
    [~,ia,ic] = intersect(od{n-2}(:,4:7),u,'rows');
    out = NaN([length(u),13]);
    out(ia,1:4) = u(ic,:);
    out(ia1,5:7) = od{n-2}(ic1,1:3);
    out(ia2,8:10) = od{n-1}(ic2,1:3);
    out(ia3,11:13) = od{n}(ic3,1:3);
    cout = num2cell(out);
    if ischar(EXPp.Costain)
        s = 1;
        Srows = out(:,4)==1;
        cout(Srows,4) = {EXPp.Costain};
    else
    for s = 1:length(COSidx)
        idx = COSidx(s);
        Srows = out(:,4)==us(s);% swap stain numbers for stain names not us(s)
        cout(Srows,4) = EXPp.Costain(idx);
    end
    end
    headers = {...
        'ImgID',...
        'CellID',...
        'DateNum',...
        'StainName',...
        ['mean_',BinGroups{i}{1},'_intensity'],['std_',BinGroups{i}{1},'_intensity'],['nFAs_',BinGroups{i}{1},'_group'],...
        ['mean_',BinGroups{i}{2},'_intensity'],['std_',BinGroups{i}{2},'_intensity'],['nFAs_',BinGroups{i}{2},'_group'],...
        ['mean_',BinGroups{i}{3},'_intensity'],['std_',BinGroups{i}{3},'_intensity'],['nFAs_',BinGroups{i}{3},'_group']};
    cout = vertcat(headers,cout);
    xlswrite(fullfile(FFCp.SaveFolder,['data_AllCells_',SaveName{i}{:},'_',FFCp.SaveName,'.xlsx']),cout);
end

% Normalize I_SF and I_LP to <I_SF> and <I_LP>
% if ~ischar(EXPp.Costain)
%     nf = [0.95,1.15,0.7];
%     for j = 1:3
%                 m = mean(GlobalQuant{j}(:,1));
%                 m = m*nf(j);
%         GlobalQuant{j}(:,1:2) = GlobalQuant{j}(:,1:2)./nf(j);
%     end
% end

%% Assemble datasets and save
% CORRELATIONS ALREADY ASSEMBLED
CCout = num2cell(GlobalQuant{end});
CCout = horzcat(EXPp.Costain',CCout);
CCout = vertcat([{'Costain'},GlobalQuantHeaders{end}],CCout);
xlswrite(fullfile(FFCp.SaveFolder,['data_CC_',FFCp.SaveName,'.xlsx']),CCout);
% mean,   upper stderr,   lower stderr,   raw p,   BC p
FFC_bar_plot(GlobalQuant{end}(:,[1,5,6,10,11]),EXPp,FFCp,'Pearson Corr. Coeff.','CCglobal_')
FFC_bar_plot(GlobalQuant{end}(:,[12,16,17,21,22]),EXPp,FFCp,'SF Pearson Corr. Coeff.','CCSFs_')
FFC_bar_plot(GlobalQuant{end}(:,[23,27,28,32,33]),EXPp,FFCp,'LP Pearson Corr. Coeff.','CCLPs_')

% Save out AP, RI & APsRI files
if strcmpi(EXPp.Construct,'VinTS')
    GQidx = {2,3,[2,3],[5,6],[8,9],[11,12]};
    SN = {'APSF','APLP','AP','RI','SFsRI','LPsRI'};
    ylab = {'SF Enrichment','LP Enrichment','Architectural Preference','Recruitment Index','SF-specific Recruitment Index','LP-specific Recruitment Index'};
else
    GQidx = {2,3,[2,3]};
    SN = {'APSF','APLP','AP'};
    ylab = {'SF Enrichment','LP Enrichment','Architectural Preference'};
end
for i = 1:length(GQidx)
    CompileAndSaveGQ(GlobalQuant(GQidx{i}),GlobalQuantHeaders(GQidx{i}),SN{i},ylab{i},EXPp,FFCp)
end

%% For a single costain, perform cell-by-cell calculations & visualizations
if ischar(EXPp.Costain)
    % Prepare to save out AP, RI & APsRI files
    if strcmpi(EXPp.Construct,'VinTS')
        cSN = {'0CC','1AP','2RI','3SFsRI','4LPsRI'};
    else
        cSN = {'0CC','1AP'};
    end
    
    for i = 1:length(cSN)
        mkdir(fullfile(FFCp.SaveFolder1,cSN{i}));
        addpath(fullfile(FFCp.SaveFolder1,cSN{i}));
    end
    
    uc = unique(d.num(:,CN.Unique.num));
    CellQuant = cell([nquant*3+1,1]);
    CellQuantHeaders = cell([nquant*3+1,1]);
    s = 0;
    for i = 1:nquant
        for j = 1:3
            s = s+1;
            CellQuant{s} = zeros([length(uc),4]);
            for n = 1:length(uc)
                Crows = d.num(:,CN.Unique.num)==uc(n);
                Brows = d.num(:,BinCols(i))==j-1;
                rows = Crows & Brows;
                if length(nonzeros(rows))==0
                    CellQuant{s}(n,1:4) = 0;
                else
                    CellQuant{s}(n,1) = mean(d.num(rows,CN.stain_intensity.num)); % mean
                    CellQuant{s}(n,2) = std(d.num(rows,CN.stain_intensity.num)); % std
                    CellQuant{s}(n,3) = length(unique(d.num(rows,CN.Unique.num))); % nCells
                    CellQuant{s}(n,4) = length(nonzeros(rows)); % nFAs
                end
                CellQuantHeaders{s} = {...
                    ['mean_',BinGroups{i}{j},'_intensity'],...
                    ['std_',BinGroups{i}{j},'_intensity'],...
                    ['nCells_',BinGroups{i}{j},'_group'],...
                    ['nFAs_',BinGroups{i}{j},'_group']};
                if s==1
                    % calculate correlation coefficient at GlobalQuant(end+1)
                    [CellQuant{end}(n,:),CellQuantHeaders{end}] = VisualizeCCcell(d.num(Crows,:),CN,FFCp,EXPp);
                end
            end
        end
    end
    for i = 2:length(cSN)
        for n = 1:length(uc)
            Crows = d.num(:,CN.Unique.num)==uc(n);
            VisualizeCellByCell(d.num(Crows,:),CN,cSN{i},EXPp,FFCp)
        end
    end
    
    %% Assemble datasets and save data and images
    % CORRELATIONS ALREADY ASSEMBLED
    CellCCout = num2cell(CellQuant{end});
    CellNames = uniqueRowsCA(d.text(:,CN.Unique.num));
    CellCCout = horzcat(CellNames,CellCCout);
    CellCCout = vertcat([{'Unique Cell'},CellQuantHeaders{end}],CellCCout);
    xlswrite(fullfile(FFCp.SaveFolder,['data_cellCC_',FFCp.SaveName,'.xlsx']),CellCCout);    
    
    % Save out AP, RI & APsRI files
    CQidx = GQidx;
    for i = 1:length(CQidx)
        CompileAndSaveCQ(CellQuant(CQidx{i}),CellQuantHeaders(CQidx{i}),SN{i},ylab{i},EXPp,FFCp,CellNames)
    end
end