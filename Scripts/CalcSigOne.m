function [d,h] = CalcSigOne(d,h,n)
% CalcSigOne calculates a number of statistical measures including the upper
% and lower confidence intervals of single log2 intensity ratio
% INPUT
% d(1) = mean #1
% d(2) = std #1
% d(3) = ncells #1
% d(4) = nFAs #1
if strcmpi(n,'FAs')
    COI = 4;
elseif strcmpi(n,'Cells')
    COI = 3;
end
% add some stats cols
lab1 = strsplit(h{1},'_');
scols = {...
    ['stderr_',lab1{2},'_intensity'],...
    ['log2(',lab1{2},'enrichment)'],...
    'err. prop. stderr',...
    'upper s.e.m.',...
    'lower s.e.m.',...
    't-statistic',...
    'p x>xbar',...
    'p x<xbar',...
    'Raw p-value',...
    'Bonf. Corr. p-value'};
d(:,5) = d(:,2)./sqrt(d(:,COI)); % stderr
d(:,6) = log2(d(:,1)); % log2(enrichment (already normalized to mean=1))
d(:,7) = (1/log(2)).*sqrt((d(:,5)./d(:,1)).^2); % error propogated standard error
d(:,8) = d(:,6)+d(:,7); % upper s.e.m.
d(:,9) = d(:,6)-d(:,7); % lower s.e.m.
d(:,10) = d(:,6)./d(:,7); % t-statistic;
d(:,11) = 1-tcdf(d(:,10),d(:,COI)-1); % p x > xbar
d(:,12) = tcdf(d(:,10),d(:,COI)-1); % p x < xbar
d(:,13) = 2.*nanmin(d(:,11:12),[],2);
d(:,14) = 22.*d(:,13);
d(d(:,14)>1,14) = 1;
h = horzcat(h,scols);
end

