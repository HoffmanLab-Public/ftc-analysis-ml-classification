function [d,h] = CalcSigTwo(d,h,n)
% CalcSig calculates a number of statistical measures including the upper
% and lower confidence intervals of an error propogated value
% INPUT
% d(1) = mean #1
% d(2) = std #1
% d(3) = ncells #1
% d(4) = nFAs #1
% d(5) = mean #2
% d(6) = std #2
% d(7) = ncells #2
% d(8) = nFAs #2

% Notes on error propogation
% for y = logb(x)
%     dy = (1/ln(b))*(dx/x)
if strcmpi(n,'FAs')
    COI = [4,8];
elseif strcmpi(n,'Cells')
    COI = [3,7];
end
% add some stats cols
lab1 = strsplit(h{1},'_');
lab2 = strsplit(h{5},'_');
scols = {...
    ['stderr_',lab1{2},'_intensity'],...
    ['stderr_',lab2{2},'_intensity'],...
    ['log2(',lab1{2},'to',lab2{2},'ratio)'],...
    'err. prop. stddev',...
    'err. prop. stderr',...
    'upper s.e.m.',...
    'lower s.e.m.',...
    't-statistic',...
    'p x>xbar',...
    'p x<xbar',...
    'Raw p-value',...
    'Bonf. Corr. p-value'};
d(:,9) = d(:,2)./sqrt(d(:,COI(1))); % stderr #1
d(:,10) = d(:,6)./sqrt(d(:,COI(2))); % stderr #2
d(:,11) = log2(d(:,1)./d(:,5)); % log2(ratio)
d(:,12) = (1/log(2)).*sqrt(((d(:,9)./d(:,1)).^2)+((d(:,6)./d(:,5)).^2)); % error propogated standard deviation
d(:,13) = d(:,12)./sqrt(mean(d(:,COI),2));% error propogated standard error
d(:,14) = d(:,11)+d(:,13); % upper s.e.m.
d(:,15) = d(:,11)-d(:,13); % lower s.e.m.
d(:,16) = d(:,11)./d(:,13); % t-statistic;
d(:,17) = 1-tcdf(d(:,16),floor(nanmean(d(:,COI),2))-1); % p x > xbar
d(:,18) = tcdf(d(:,16),floor(nanmean(d(:,COI),2))-1); % p x < xbar
d(:,19) = 2.*nanmin(d(:,17:18),[],2);
d(:,20) = 22.*d(:,19);
d(d(:,20)>1,20) = 1;
h = horzcat(h,scols);
end

