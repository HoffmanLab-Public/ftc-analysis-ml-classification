function [d,CN] = CalcStainIntensity(d,CN,FFCp)
%CalcStainIntensity Adds a 'stain intensity' column to d based on the
%requested stain intensity (y-axis in RI and AP plots) in FFCparams
%structure
cellfind = @(string)(@(cell_contents)(strcmp(string,cell_contents)));
COLNAMES_SHORT = importdata('COLNAMES_SHORT.csv');
COLNAMES_SHORT = strsplit(COLNAMES_SHORT{:},',');

%% Calculate ExpNormCy5 Intensity
% Calculate normalized Cy5 intensity on an experiment-by-experiment basis
% (essentially normalizing all stain intensities in a single dish to 1)
CN.npre.text = 'normalized pre Mean';
CN.npre.num = CN.nCols+1;
CN.nCols = CN.nCols+1;
d.headers{end+1} = 'normalized pre Mean';
d.num(:,CN.npre.num) = 0; % preallocate space for normalized pre intensities
ue = unique(d.num(:,CN.UniqueExp.num));
for e = 1:length(ue)
    rows = d.num(:,CN.UniqueExp.num)==ue(e);
    normfactor = mean(d.num(rows,CN.pre.num));
    d.num(rows,CN.npre.num) = d.num(rows,CN.pre.num)./normfactor;
end
d.text = horzcat(d.text,cell(length(d.text),1));
clear e rows normfactor

%% Calculate CellNormCy5 Intensity
% Calculate normalized Cy5 intensity on a cell-by-cell basis
% (essentially normalizing all stain intensities in a single cell to 1)
CN.nCpre.text = 'normalized Cell pre Mean';
CN.nCpre.num = CN.nCols+1;
CN.nCols = CN.nCols+1;
d.headers{end+1} = 'normalized Cell pre Mean';
d.num(:,CN.nCpre.num) = 0; % preallocate space for normalized pre intensities
uc = unique(d.num(:,CN.Unique.num));
for c = 1:length(uc)
    rows = d.num(:,CN.Unique.num)==uc(c);
    normfactor = mean(d.num(rows,CN.pre.num));
    d.num(rows,CN.nCpre.num) = d.num(rows,CN.pre.num)./normfactor;
end
d.text = horzcat(d.text,cell(length(d.text),1));
clear c rows normfactor

%% Define stain_intensity column
% Calculate what we're plotting on the Y axis
% options: Cy5,      Cy5/Venus,      Cy5/Size,     Cy5/Venus*Size,
%          nCy5,     nCy5/Venus,     nCy5/Size,     nCy5/Venus*Size,
%          nCellCy5, nCellCy5/Venus, nCellCy5/Size, nCellCy5/Venus*Size,
%          or other parameters like Area, AR, eff, etc...
CN.stain_intensity.text = FFCp.StainIntensity;
CN.stain_intensity.num = CN.nCols+1;
CN.nCols = CN.nCols+1;
d.headers{end+1} = FFCp.StainIntensity;
d.num(:,CN.stain_intensity.num) = 0; % preallocate space for normalized pre intensities
if isempty(cellfun(cellfind(FFCp.StainIntensity),COLNAMES_SHORT))
    d.num(:,CN.stain_intensity.num) = d.num(:,CN.(FFCp.StainIntensity).num);
else
    switch FFCp.StainIntensity
        case 'Cy5'
            d.num(:,CN.stain_intensity.num) = d.num(:,CN.pre.num);
        case 'Cy5/Venus'
            d.num(:,CN.stain_intensity.num) = d.num(:,CN.pre.num)./d.num(:,CN.bsa.num);
        case 'Cy5/Size'
            d.num(:,CN.stain_intensity.num) = d.num(:,CN.pre.num)./d.num(:,CN.FAsize.num);
        case 'Cy5/Venus*Size'
            d.num(:,CN.stain_intensity.num) = d.num(:,CN.pre.num)./(d.num(:,CN.bsa.num).*d.num(:,CN.FAsize.num));
        case 'nCy5'
            d.num(:,CN.stain_intensity.num) = d.num(:,CN.npre.num);
        case 'nCy5/Venus'
            d.num(:,CN.stain_intensity.num) = d.num(:,CN.npre.num)./d.num(:,CN.bsa.num);
        case 'nCy5/Size'
            d.num(:,CN.stain_intensity.num) = d.num(:,CN.npre.num)./d.num(:,CN.FAsize.num);
        case 'nCy5/Venus*Size'
            d.num(:,CN.stain_intensity.num) = d.num(:,CN.npre.num)./(d.num(:,CN.bsa.num).*d.num(:,CN.FAsize.num));
        case 'nCellCy5'
            d.num(:,CN.stain_intensity.num) = d.num(:,CN.nCpre.num);
        case 'nCellCy5/Venus'
            d.num(:,CN.stain_intensity.num) = d.num(:,CN.nCpre.num)./d.num(:,CN.bsa.num);
        case 'nCellCy5/Size'
            d.num(:,CN.stain_intensity.num) = d.num(:,CN.nCpre.num)./d.num(:,CN.FAsize.num);
        case 'nCellCy5/Venus*Size'
            d.num(:,CN.stain_intensity.num) = d.num(:,CN.nCpre.num)./(d.num(:,CN.bsa.num).*d.num(:,CN.FAsize.num));
        otherwise
            error('Choose a valid stain intensity to plot on the Y axis')
    end
end
d.text = horzcat(d.text,cell(length(d.text),1));
end

