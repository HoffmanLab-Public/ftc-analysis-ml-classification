function d = CleanData(d,CN,EXPp,FFCp)
%CleanData Cleans up the dataset of interest based on
%          universal exclusiion criteria
cellfind = @(string)(@(cell_contents)(strcmp(string,cell_contents)));

%% Exclude VinTS or VinTSI997A cells
CONrows = cellfun(cellfind(EXPp.Construct),d.text(:,CN.CON.num));
nFAsTOT = length(d.text);
d.text = d.text(CONrows,:);
d.num(~CONrows,:) = [];
disp([num2str((length(nonzeros(~CONrows))/nFAsTOT)*100,'%2.4f'),' percent of data were not ',EXPp.Construct, ' cells and were therefore excluded']);
disp(' ');

%% Exclude data outside Costains of interest
if ~ischar(EXPp.Costain)
    COSrows = zeros([length(d.text),length(EXPp.Costain)]);
    for i = 1:length(EXPp.Costain)
        COSrows(:,i) = cellfun(cellfind(EXPp.Costain{i}),d.text(:,CN.COS.num));
    end
    COSrows = max(COSrows,[],2);
    nFAsTOT = length(d.text);
    COSrows = logical(COSrows);
    d.text = d.text(COSrows,:);
    d.num(~COSrows,:) = [];
    disp([num2str((length(nonzeros(~COSrows))/nFAsTOT)*100,'%2.4f'),' percent of data were not costains of interest and were therefore excluded']);
    disp(' ');
end

%% Exclude FAs that should be ignored in this analysis
exrows1 = d.num(:,CN.FAdcc.num)<FFCp.CentralFAdist;
disp([num2str((length(nonzeros(exrows1))/length(d.text))*100,'%2.4f'),' percent of FAs were estimated to be sub-nuclear and were therefore excluded']);
exrows2 = d.num(:,CN.eff.num)<FFCp.FAeffRange(1);
disp([num2str((length(nonzeros(exrows2))/length(d.text))*100,'%2.4f'),' percent of FAs exhibit exceedingly low FRET and were therefore excluded']);
exrows3 = d.num(:,CN.eff.num)>FFCp.FAeffRange(2);
disp([num2str((length(nonzeros(exrows3))/length(d.text))*100,'%2.4f'),' percent of FAs exhibit exceedingly high FRET and were therefore excluded']);
exrows4 = d.num(:,CN.dpa.num)<FFCp.FAdpaRange(1);
disp([num2str((length(nonzeros(exrows4))/length(d.text))*100,'%2.4f'),' percent of FAs exhibit exceedingly low DPA and were therefore excluded']);
exrows5 = d.num(:,CN.dpa.num)>FFCp.FAdpaRange(2);
disp([num2str((length(nonzeros(exrows5))/length(d.text))*100,'%2.4f'),' percent of FAs exhibit exceedingly high DPA and were therefore excluded']);
exrows = exrows1 | exrows2 | exrows3 | exrows4 | exrows5;
disp(' ');
disp(['Overall, ', num2str((length(nonzeros(exrows))/length(d.text))*100,'%2.4f'),' percent of FAs were excluded from CC_AP_RI_APsRI calculations']);
disp(' ');
d.num(exrows,:) = [];
d.text = d.text(~exrows,:);
clear c uc rows avgeff avgdpa exrows1 exrows2 exrows3 exrows4 exrows5 exrows

%% Exclude cells that should be ignored in this analysis
uc = unique(d.num(:,CN.Unique.num));
count = 0;
for c = 1:length(uc)
    rows = d.num(:,CN.Unique.num)==uc(c);
    avgeff = mean(d.num(rows,CN.eff.num));
    avgdpa = mean(d.num(rows,CN.dpa.num));
    if length(nonzeros(rows))<FFCp.nFAsPerCellMin
        d.num(rows,:) = [];
        d.text = d.text(~rows,:);
        count = count+1;
    elseif avgeff<FFCp.CELLeffRange(1) || avgeff>FFCp.CELLeffRange(2) || avgdpa<FFCp.CELLdpaRange(1) || avgdpa>FFCp.CELLdpaRange(2)
        d.num(rows,:) = [];
        d.text = d.text(~rows,:);
        count = count+1;
    end
end
disp([num2str(count),' of ' num2str(length(uc)),' ',EXPp.Construct,' cells were excluded for eff or dpa outside of acceptable ranges or having too few FAs']);
disp(' ');
end

