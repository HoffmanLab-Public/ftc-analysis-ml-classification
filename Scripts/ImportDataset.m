function [d,CN] = ImportDataset(EXPp,FFCp)
%ImportDataset Imports the dataset of interst, determines the locations of
%the columns of interest as well as the predictors to be used in the
%classification scheme before Architectural Preference calculation
cellfind = @(string)(@(cell_contents)(strcmp(string,cell_contents)));

%% Import dataset
if ischar(EXPp.Costain)
    d_file = file_search('\w+compiled\w+.xlsx',FFCp.folder);
%     d_raw = importdata(d_file{1});
    [d.num,d.text,~] = xlsread(d_file{1});
else
    d_file = file_search('\w+v1.xlsx',FFCp.folder);
%     d_raw = importdata(d_file{1});
    [d.num,d.text,~] = xlsread(d_file{1});
end
% d.num = d_raw.data;
d.headers = d.text(1,:);
d.text = d.text(2:end,:);
% d.text = d_raw.textdata(2:end,:);
% d.headers = d_raw.textdata(1,:);
[~,cnum] = size(d.num);
[~,ctext] = size(d.text);

% Equalize size of d.num & d.text
dnumtext = abs(cnum-ctext);
if cnum<ctext
    d.num(:,end+1:end+dnumtext) = NaN;
elseif cnum>ctext
    d.text = horzcat(d.text,cell([length(d.text),dnumtext]));
end

%% Find columns of interest
COLNAMES = importdata('COLNAMES.csv');
COLNAMES = strsplit(COLNAMES{:},',');
COLNAMES_SHORT = importdata('COLNAMES_SHORT.csv');
COLNAMES_SHORT = strsplit(COLNAMES_SHORT{:},',');

% Figure out which column numbers apply to the col names above
for c = 1:length(COLNAMES)
    CN.(COLNAMES_SHORT{c}).text = COLNAMES{c};
    CN.(COLNAMES_SHORT{c}).num = find(cellfun(cellfind(CN.(COLNAMES_SHORT{c}).text),d.headers));
end
CN.nCols = length(d.headers);

%% Add Unique Stain, Unique Experiment, & Unique Cell columns
% Unique stain ID is already halfway completed
% Text already assigned
% Assign number
UniqueStains = uniqueRowsCA(d.text(:,CN.COS.num));
for i = 1:length(UniqueStains)
    rows = cellfun(cellfind(UniqueStains{i}),d.text(:,CN.COS.num));
    d.num(rows,CN.COS.num) = i;
end

% Calculate a unique experiment ID
% Assign text
CN.UniqueExp.text = 'UniqueExperiment';
d.headers{end+1} = 'UniqueExperiment';
CN.UniqueExp.num = CN.nCols+1;
CN.nCols = CN.nCols+1;
d.text(:,CN.UniqueExp.num) = strcat(...
    d.text(:,CN.Date.num),'-',...
    d.text(:,CN.CON.num),'-',...
    d.text(:,CN.COS.num));
% Assign number
d.num(:,end+1) = 0;
UniqueExperiments = uniqueRowsCA(d.text(:,CN.UniqueExp.num));
for i = 1:length(UniqueExperiments)
    rows = cellfun(cellfind(UniqueExperiments{i}),d.text(:,CN.UniqueExp.num));
    d.num(rows,CN.UniqueExp.num) = i;
end

% Calculate a unique cell ID
% Assign text
CN.Unique.text = 'Unique';
d.headers{end+1} = 'UniqueCell';
CN.Unique.num = CN.nCols+1;
CN.nCols = CN.nCols+1;
d.text(:,CN.Unique.num) = strcat(...
    d.text(:,CN.Date.num),'-',...
    d.text(:,CN.CON.num),'-',...
    d.text(:,CN.COS.num),'-img',...
    num2str(d.num(:,CN.ImgID.num),'%02.f'),'-cell',...
    num2str(d.num(:,CN.CellID.num),'%02.f'));
% Assign number
d.num(:,CN.Unique.num) = 0;
d.num(:,CN.Unique.num) = ...
    d.num(:,CN.UniqueExp.num)*10^6 + ...
    d.num(:,CN.ImgID.num)*10^2 + ...
    d.num(:,CN.CellID.num)*10^0;

%% Define predictors to be used in APcalc
for i = 1:length(FFCp.Predictors)
    CN.pCols(i) = CN.(FFCp.Predictors{i}).num;
end
end

