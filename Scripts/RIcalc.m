function [d,CN] = RIcalc(d,CN,RIq)
%RI calc adds RI column to "d" based on RIquartiles

eff = d.num(:,CN.eff.num);
HighForceRows = eff>RIq(1) & eff<RIq(2);
IntForceRows = eff>=RIq(2) & eff<=RIq(4);
LowForceRows = eff>RIq(4) & eff<RIq(5);

%% Add column for RI bin
d.text(HighForceRows,end+1) = {'LowFRET'};
d.num(HighForceRows,end+1) = 1;
d.text(IntForceRows,end) = {'MediumFRET'};
d.num(IntForceRows,end) = 0;
d.text(LowForceRows,end) = {'HighFRET'};
d.num(LowForceRows,end) = 2;
d.headers{end+1} = 'FRETbins';
[~,CN.FRETbins.num] = size(d.num);
CN.FRETbins.text = 'Binned FRET efficiency';
end

