function FFCp = RIcalcPrep(d,CN,FFCp)
%RIcalcPrep Prepares the dataset for recruitment index calculations
%            with the following operations
%            (1) Calculate global RI quartiles

%% Calculate global RI quartiles
FFCp.RIquartiles = quantile(d.num(:,CN.eff.num),3);
FFCp.RIquartiles = [FFCp.FAeffRange(1) FFCp.RIquartiles FFCp.FAeffRange(2)];
end